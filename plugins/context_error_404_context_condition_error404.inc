<?php

class context_error_404_context_condition_error404 extends context_condition {

/**
 * condition_values() method.
*/

  function condition_values() {
    $values = array();
    $values['404'] = t('Error 404');
    return $values;
  }

/**
 * execute() method.
*/

  function execute() {
     foreach ($this->get_contexts() as $context) {
	  if (!menu_get_item()){
        $this->condition_met($context);
      } 
     }
  }
}
